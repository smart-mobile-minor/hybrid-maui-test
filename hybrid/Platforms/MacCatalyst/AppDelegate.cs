﻿using Foundation;
using Microsoft.Maui;

namespace hybrid
{
	[Register("AppDelegate")]
	public class AppDelegate : MauiUIApplicationDelegate<Startup>
	{
	}
}